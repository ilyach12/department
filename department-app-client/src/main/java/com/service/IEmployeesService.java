package com.service;

import com.model.Employees;

import java.sql.Date;
import java.util.List;

public interface IEmployeesService {

    List<Employees> findAll();
    List<Employees> findEmployeesByBirthdayDate(Date birthday);
    List<Employees> findEmployeesByBirthdayDateBetween(Date birthday, Date birthday1);
    List<Employees> findEmployeesByDepartmentName(String departmentName);
    void insertNewEmployee(String fullName, String department,
                           Date birthday, int salary);
    void updateEmployeeById(Long id, String fullName, String department,
                            Date birthday, int salary);
    void deleteEmployeeById(Long id);
}
