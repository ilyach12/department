package com.service;

import com.model.Department;

import java.util.List;

public interface IDepartmentsService {

    List<Department> findAll();
    List<Department> findDepartmentByNameWithEmployees(String departmentName);
    List<Department> findAllDepartmentsWithEmployees();
    void insertNewDepartment(String departmentName);
    void updateDepartmentById(Long id, String departmentName);
    void deleteDepartmentByName(String departmentName);
}
