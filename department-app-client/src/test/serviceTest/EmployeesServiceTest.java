package serviceTest;

import com.model.Employees;
import com.service.EmployeesService;
import org.junit.Test;

import java.sql.Date;
import java.util.List;

import static org.junit.Assert.assertEquals;

public class EmployeesServiceTest {

    private EmployeesService employeesService;

    @Test
    public void testFindAll(){
        employeesService = new EmployeesService();
        List<Employees> employees = employeesService.findAll();
        assertEquals(16, employees.size());
    }

    @Test
    public void testFindEmployeesByBirthday(){
        employeesService = new EmployeesService();
        List<Employees> employees = employeesService.findEmployeesByBirthdayDate(Date.valueOf("1997-08-11"));
        assertEquals(1, employees.size());
    }

    @Test
    public void testFindEmployeesByBirthdayBetween(){
        employeesService = new EmployeesService();

        String birthday = "1993-01-01";
        String birthday1 = "1997-01-01";
        List<Employees> employees = employeesService.findEmployeesByBirthdayDateBetween(Date.valueOf(birthday),
                Date.valueOf(birthday1));
        assertEquals(5, employees.size());
    }

    @Test
    public void testFindEmployeesByDepartmentName(){
        employeesService = new EmployeesService();
        List<Employees> employees = employeesService.findEmployeesByDepartmentName("dotNOT");
        assertEquals(4, employees.size());
    }

    @Test
    public void testInsertEmployee(){
        employeesService = new EmployeesService();
        employeesService.insertNewEmployee("Evkakiy", "dotNOT", Date.valueOf("1974-02-17"), 700);

        List<Employees> employees = employeesService.findAll();
        assertEquals(17, employees.size());
        List<Employees> employeesBday = employeesService.findEmployeesByBirthdayDate(Date.valueOf("1974-02-17"));
        assertEquals(1, employeesBday.size());
    }

    @Test
    public void testUpdateEmployeeById(){
        employeesService = new EmployeesService();
        employeesService.updateEmployeeById(54L, "Artemiy", "Java", Date.valueOf("1920-01-01"), 800);

        List<Employees> employeesBday = employeesService.findEmployeesByBirthdayDate(Date.valueOf("1920-01-01"));
        assertEquals(1, employeesBday.size());
    }

    @Test
    public void testDeleteEmployeeById(){
        employeesService = new EmployeesService();
        employeesService.deleteEmployeeById(54L);

        List<Employees> employees = employeesService.findAll();
        assertEquals(16, employees.size());
    }
}
