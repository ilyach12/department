package serviceTest;

import com.model.Department;
import com.service.DepartmentsService;
import org.junit.Before;
import org.junit.Test;

import java.util.ArrayList;
import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

public class DepartmentsServiceTest {

    private DepartmentsService departmentService;

    private List<Department> list = new ArrayList<>();

    @Before
    public void initDepartment(){
        Department department = new Department();
        department.setId(1L);
        department.setDepartmentName("Java");
        department.setEmployeesInThisDepartment(new ArrayList<>());
        department.setAverageSalary(800);

        Department department1 = new Department();
        department1.setId(2L);
        department1.setDepartmentName(".NET");
        department1.setEmployeesInThisDepartment(new ArrayList<>());
        department1.setAverageSalary(800);

        list.add(department);
        list.add(department1);
    }

    @Test
    public void testFindAll(){
        departmentService = mock(DepartmentsService.class);
        when(departmentService.findAll()).thenReturn(list);
        assertEquals(2, departmentService.findAll().size());
    }

    @Test
    public void testFindAllWithEmployees(){
        departmentService = mock(DepartmentsService.class);
        when(departmentService.findAllDepartmentsWithEmployees()).thenReturn(list);
        assertEquals(2, departmentService.findAllDepartmentsWithEmployees().size());
    }

    @Test
    public void testFindDepartmentByNameWithEmployees(){
        departmentService = new DepartmentsService();
        List<Department> department = departmentService.findDepartmentByNameWithEmployees("Java");
        assertEquals(1, department.size());
    }

    @Test
    public void testInsertNewDepartment(){
        departmentService = new DepartmentsService();
        departmentService.insertNewDepartment("Test");
        List<Department> departments = departmentService.findAll();
        assertEquals(6, departments.size());
        List<Department> department = departmentService.findDepartmentByNameWithEmployees("Test");
        assertEquals(1, department.size());

    }

    @Test
    public void testUpdateDepartmentById(){
        departmentService = new DepartmentsService();
        departmentService.updateDepartmentById(107L, "Testing");
        List<Department> departments = departmentService.findAll();
        assertEquals(6, departments.size());
        List<Department> department = departmentService.findDepartmentByNameWithEmployees("Testing");
        assertEquals(1, department.size());
    }

    @Test
    public void testDeleteDepartmentByName(){
        departmentService = new DepartmentsService();
        departmentService.deleteDepartmentByName("Testing");
        List<Department> departments = departmentService.findAll();
        assertEquals(5, departments.size());
    }
}
