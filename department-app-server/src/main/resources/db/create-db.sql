CREATE TABLE DEPARTMENT
(
    id BIGINT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    DEPARTMENTNAME CHAR NOT NULL
);
CREATE UNIQUE INDEX "DEPARTMENT_id_uindex" ON DEPARTMENT (id);
CREATE UNIQUE INDEX "DEPARTMENT_DEPARTMENTNAME_uindex" ON DEPARTMENT (DEPARTMENTNAME);

CREATE TABLE EMPLOYEES
(
    id BIGINT AUTO_INCREMENT PRIMARY KEY NOT NULL,
    DEPARTMENT CHAR NOT NULL,
    FULLNAME CHAR NOT NULL,
    BIRTHDAY DATE,
    SALARY INT NOT NULL
);
