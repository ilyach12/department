package com.model;

import java.io.Serializable;
import java.util.List;

/**
 * This is a domain class for Department data table
 */
public class Department implements Serializable{

    private Long id;
    private String departmentName;
    private List<Employees> employeesInThisDepartment;
    private int averageSalary;
    private List<Integer> salaryList;

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getDepartmentName() {
        return departmentName;
    }

    public void setDepartmentName(String departmentName) {
        this.departmentName = departmentName;
    }

    public List<Employees> getEmployeesInThisDepartment() {
        return employeesInThisDepartment;
    }

    public void setEmployeesInThisDepartment(List<Employees> employeesInThisDepartment) {
        this.employeesInThisDepartment = employeesInThisDepartment;
    }

    public int getAverageSalary() {
        return averageSalary;
    }

    public void setAverageSalary(int averageSalary) {
        this.averageSalary = averageSalary;
    }

    public List<Integer> getSalaryList() {
        return salaryList;
    }

    public void setSalaryList(List<Integer> salaryList) {
        this.salaryList = salaryList;
    }
}
