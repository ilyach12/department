package com.service;

import com.model.Employees;
import com.dao.JdbcEmployeesDao;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.sql.Date;
import java.util.List;

/**
 * This class annotated as "Service" and him has role of protection layout
 * between {@code EmployeesController} controller and {@code JdbcEmployeesDao} data
 * access class.
 */
@Service("employeesService")
public class EmployeesService implements IEmployeesService {

    /**
     * Dependency injection of {@code JdbcEmployeesDao} class.
     */
    @Autowired
    private JdbcEmployeesDao jdbcEmployeesDao;

    @Override
    public List<Employees> getAll() {
        return jdbcEmployeesDao.findAll();
    }

    @Override
    public List<Employees> getEmployeesByBirthdayDate(Date birthday) {
        return jdbcEmployeesDao.findByBirthdayDate(birthday);
    }

    @Override
    public List<Employees> getEmployeesByDepartmentName(String departmentName) {
        return jdbcEmployeesDao.findByDepartmentName(departmentName);
    }

    @Override
    public List<Employees> getEmployeesByBirthdayDateBetween(Date birthday, Date birthday1) {
        return jdbcEmployeesDao.findByBirthdayBetween(birthday, birthday1);
    }

    @Override
    public void insert(String employeeName, String department, Date birthday, int salary) {
        jdbcEmployeesDao.insertNewEmployee(employeeName, department, birthday, salary);
    }

    @Override
    public void update(Long id, String employeeName, String department, Date birthday, int salary) {
        jdbcEmployeesDao.updateById(id, employeeName, department, birthday, salary);
    }

    @Override
    public void delete(Long id) {
        jdbcEmployeesDao.deleteById(id);
    }
}
