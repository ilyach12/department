package com.service;

import com.dao.JdbcDepartmentsDao;
import com.model.Department;
import com.model.Employees;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;

/**
 * This class annotated as "Service" and him has role of protection layout
 * between {@code DepartmentController} controller and {@code JdbcDepartmentsDao} data
 * access class.
 */
@Service("departmentService")
public class DepartmentsService implements IDepartmentsService {

    @Autowired
    private JdbcDepartmentsDao departmentDao;

    @Override
    public List<Department> getAll(){
        return departmentDao.findAll();
    }

    @Override
    public List<Department> getAllDepartmentsWithEmployees() {
        return departmentDao.findAllWithEmployees();
    }

    @Override
    public List<Department> getDepartmentByNameWithEmployees(String departmentName){
        return departmentDao.findDepartmentByNameWithEmployees(departmentName);
    }

    @Override
    public void update(Long id, String departmentName){
        departmentDao.updateById(id, departmentName);
    }

    @Override
    public void delete(String departmentName){
        departmentDao.deleteByName(departmentName);
    }

    @Override
    public void insert(String departmentName){
        departmentDao.insertNewDepartment(departmentName);
    }
}
