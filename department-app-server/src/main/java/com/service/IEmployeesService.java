package com.service;

import com.model.Employees;

import java.sql.Date;
import java.util.List;

public interface IEmployeesService {

    List<Employees> getAll();
    List<Employees> getEmployeesByDepartmentName(String departmentName);
    List<Employees> getEmployeesByBirthdayDate(Date birthday);
    List<Employees> getEmployeesByBirthdayDateBetween(Date birthday, Date birthday1);
    void insert(String employeeName, String department,
                Date birthday, int salary);
    void update(Long id, String employeeName, String department,
                Date birthday, int salary);
    void delete(Long id);
}
